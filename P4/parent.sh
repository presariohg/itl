#!/usr/bin/bash
./child_one.sh &
./child_two.sh &

trap "{ echo 'Parent terminated'; exit; }" SIGTERM
while true
do
    sleep 5
done
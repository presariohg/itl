cmake_minimum_required(VERSION 3.1)
project(hello_world)

set(CMake_CXX_STANDARD 17)
set(HelloWorldSources src/hello_world.cpp src/MyLibrary.cpp)
set(HelloWorldHeaders include/MyLibrary.h)

add_library(mylibrary SHARED ${HelloWorldSources} ${HelloWorldHeaders})

target_include_directories(mylibrary PUBLIC include)

find_package(Boost REQUIRED COMPONENTS thread)

add_executable(hello_world ${HelloWorldSources})

target_link_libraries(hello_world   PRIVATE Boost::thread
                                    PRIVATE mylibrary)
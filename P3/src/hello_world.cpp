#include <iostream>
#include <boost/thread/thread.hpp>
#include <MyLibrary.h>

int main() {
    boost::thread sample_thread(hello);
    sample_thread.join();
    return 0;
}

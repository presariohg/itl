args_count=$#;
# echo "${args_count}"

case $args_count in
    0)
        echo "Get some --help please"
    ;;

    1)
        operator=$1;
        case $operator in 
            add)
                read -p "First integer: " a;
                read -p "Second integer: " b;
                echo "$a + $b = `expr $a + $b`"
            ;;

            sub)
                read -p "First integer: " a;
                read -p "Second integer: " b;
                echo "$a - $b = `expr $a - $b`"
            ;;

            mult)
                read -p "First integer: " a;
                read -p "Second integer: " b;
                echo "$a x $b = `expr $a * $b`"
            ;;
            
            div)
                read -p "First integer: " a;
                read -p "Second integer: " b;
                echo "$a / $b = `expr $a / $b`"
            ;;

            cross)
                read -p "Input: " n;
                string_length=${#n};
                cross_sum=0;
                for ((i=0; i < string_length; i++)); do
                    cross_sum=`expr $cross_sum + ${n:i:1}`;
                done

                echo "Cross sum: $cross_sum";
            ;;

            "--help")
                echo "Read the source code yourself please :)"
            ;;

            *)
                echo "Unrecognized argument $operator, get some --help please"
            ;;
        esac
    ;;

    2)
        operator=$1;
        if [ $operator == "cross" ]; then
            n=$2;            
            string_length=${#n};
            cross_sum=0;
            for ((i=0; i < string_length; i++)); do
                cross_sum=`expr $cross_sum + ${n:i:1}`;
            done

            echo "Cross sum $n: $cross_sum";
        else
            echo "Unrecognized argument $operator, get some --help please"
        fi
    ;;

    3)
        operator=$1;
        a=$2;
        b=$3;
        case $operator in 
            add)
                echo "$a + $b = `expr $a + $b`"
            ;;

            sub)
                echo "$a - $b = `expr $a - $b`"
            ;;

            mult)
                echo "$a x $b = `expr $a \* $b`"
            ;;
            
            div)
                echo "$a / $b = `expr $a / $b`"
            ;;

            *)
                echo "Unrecognized argument $operator, get some --help please"
            ;;
        esac
esac
# echo `ls *.pub` | sed 's/.pub//g';
for file in *; do
    if [[ "$file" == *".pub"* ]]; then
        echo `echo $file | sed "s/.pub//g"`;
    fi
done
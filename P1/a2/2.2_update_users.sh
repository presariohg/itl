users=""
for file in *; do
    if [[ "$file" == *".pub"* ]]; then
        users+=`echo $file | sed "s/.pub/ /g"`;
    fi
done
echo $users;
sed -i "s|@demo_project_users = .*|@demo_project_users = $users|g" example.conf

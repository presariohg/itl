echo "loop even:"
for i in {0..10}; do
    if [ $(expr $i % 2) == 0 ]; then
        echo $i
    fi
done